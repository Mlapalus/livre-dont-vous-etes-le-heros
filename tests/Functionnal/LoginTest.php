<?php


namespace App\Tests\Functionnal;


use Symfony\Component\HttpFoundation\Response;

class LoginTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    public function test_if_login_is_successfull()
    {
        $client = static::createClient();
        $crawler = $client->request("GET", "/login");

        $form = $crawler->filter("form[name=login]")->form([
            "email" => "user@email.com",
            "password" => "password"
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $client->followRedirect();
        $this->assertRouteSame("index");
    }

    public function test_if_fail_when_bad_email()
    {
        $client = static::createClient();
        $crawler = $client->request("GET", "/login");

        $form = $crawler->filter("form[name=login]")->form([
            "email" => "bad@email.com",
            "password" => "password"
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $client->followRedirect();
        $this->assertRouteSame("security_login");
    }

    public function test_if_fail_when_bad_password()
    {
        $client = static::createClient();
        $crawler = $client->request("GET", "/login");

        $form = $crawler->filter("form[name=login]")->form([
            "email" => "user@email.com",
            "password" => "badPassword"
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $client->followRedirect();
        $this->assertRouteSame("security_login");
    }
    public function test_if_fail_when_password_is_empty()
    {
        $client = static::createClient();
        $crawler = $client->request("GET", "/login");

        $form = $crawler->filter("form[name=login]")->form([
            "email" => "user@email.com",
            "password" => ""
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $client->followRedirect();
        $this->assertRouteSame("security_login");
    }

    public function test_if_fail_when_email_is_empty()
    {
        $client = static::createClient();
        $crawler = $client->request("GET", "/login");

        $form = $crawler->filter("form[name=login]")->form([
            "email" => "",
            "password" => "password"
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $client->followRedirect();
        $this->assertRouteSame("security_login");
    }

    public function test_if_fail_when_bad_CsrfToken()
    {
        $client = static::createClient();
        $crawler = $client->request("GET", "/login");

        $form = $crawler->filter("form[name=login]")->form([
            "email" => "user@email.com",
            "password" => "password",
            "_csrf_token" => "fail"
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $client->followRedirect();
        $this->assertRouteSame("security_login");
    }
}